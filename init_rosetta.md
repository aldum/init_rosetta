# Init Rosetta
**Regardless of what init system and what operating system you use, please make
sure to read the [notes](#notes) below


| action | systemd | openRC | runit | s6 |
|--------|---------|--------|-------|----|
| list services | `systemctl list-unit-files` | `rc-status` |  | `s6-rc-db list all` |
| list running services' status | `systemctl list-units` | `rc-update show` | `sv status` | `s6-rc -a list` |
| list available services | `systemctl --all` | `rc-update -v show` or `ls /etc/init.d/` | `ls /etc/runit/sv` | `s6-rc-db list all` |
| list failed services | `systemctl --failed` | `rc-status --crashed` |  | `s6-rc -ad list` <br> see [notes](#general) |
| start a service | `systemctl start [SERVICE_NAME]`   | `rc-service [SERVICE_NAME] start` or `/etc/init.d/[SERVICE_NAME] start` | `sv up [SERVICE_NAME]` or `sv u [SERVICE_NAME]` | `s6-rc -u change [SERVICE_NAME]` |
| stop a service | `systemctl stop [SERVICE_NAME]`    | `rc-service [SERVICE_NAME] stop` or `/etc/init.d/[SERVICE_NAME] stop` | `sv down [SERVICE_NAME]` or `sv d [SERVICE_NAME]` | `s6-rc -d change [SERVICE_NAME]` |
| restart a service | `systemctl restart [SERVICE_NAME]` | `rc-service [SERVICE_NAME] restart` or `/etc/init.d/[SERVICE_NAME] restart`  | `sv restart [SERVICE_NAME]` or `sv t [SERVICE_NAME]` | `s6-svc -r /run/s6-rc/servicedirs/[SERVICE_NAME]` |
| get the status of a service  | `systemctl status [SERVICE_NAME]`  | `rc-service [SERVICE_NAME] status` or `/etc/init.d/[SERVICE_NAME] status` | `sv status [SERVICE_NAME]` or `sv s [SERVICE_NAME]` | `s6-svstat /run/s6-rc/servicedirs/[SERVICE_NAME]` |
| enable a service | `systemctl enable [SERVICE_NAME]`  | `rc-update add [SERVICE_NAME]` | `ln -s /etc/sv/[SERVICE_NAME] /var/service` | `s6-rc-bundle-update add [BUNDLE] [SERVICE_NAME]` <br> see [notes](#general) |
| disable a service | `systemctl disable [SERVICE_NAME]` | `rc-update del [SERVICE_NAME]` | `rm /var/service/[SERVICE_NAME]` | `s6-rc-bundle-update delete [BUNDLE] [SERVICE_NAME]` |


## Notes

### runit

#### For Artix users

* available services are in `/etc/runit/sv/` directory
* enabled services are in `/run/runit/service/` directory
    * note: this is when your system is running, if you are trying to fix a
    system via chrooting, you can find the services in the appropriate runlevel
    folder (likely `default`) under `/etc/runit/runsvdir/`


### s6

#### General

* most of the commands listed are actually the commands from `s6-rc`, which
works on top of `s6`; you should generally avoid touching `s6` itself, unless
it's suggested here, or you really know what you're doing
* `s6-rc` does not implement "failed" state; only "up" and "down" states are
supported
* when disabling a service, keep in mind you should also disable all the
services that depend on it, otherwise the service manager will restart the
disabled service as a dependency of another service
* all the commands suggested for manually enabling/disabling a service do not
make any sanity checks, so make sure you've done all the necessary backups
before running them
   * do not use the variable name that is already in use by something else
   * consider dumping the value of `[VAR]` somewhere before unsetting it
   to be able to recompile the database in case of any errors

- `[SERVICE_REPO]` is the absolute path to your service repository
for `s6-svscan`
    * refer to your system documentation, if you don't know this path
- `[SCANDIR]` is the absolute path to your scan directory for `s6-svscan`
    * run `pstree -a | head -n1`; the last argument of command in the output
    should be the path you  need
- `[VAR]` is an arbitrary variable to store previous contents of the bundle
- `[DATABASE]` is the absolute path to the compiled database
    * refer to your system documentation, if you don't know this path
- `[BUNDLE]` is the name of bundle, most likely the top level one
    * a bundle is like a runlevel or systemd target,
        in most cases, you would just use "default"
    * refer to your system documentation, if you don't know the bundle name
    * issuing `s6-rc-db -d all-dependencies [SERVICE_NAME]` might help, as it
    lists all the services that depend on `[SERVICE_NAME]`, the top level
    bundle included, you can try making something up from there


##### For Artix users

* all commands listed require `sudo` privileges by default
* to enable/disable a service, use the helper script `s6-rc-bundle-update`:
```sh
s6-rc-bundle-update add default [SERVICE_NAME]              #enable
s6-rc-bundle-update delete default [SERVICE_NAME]           #disable
```

- `[SERVICE_REPO]` is `/etc/s6/sv/` directory
- `[SCANDIR]` is `/run/service/` directory
- `[VAR]` is an arbitrary variable to store previous contents of the bundle
- `[DATABASE]` is `/run/s6-rc/compiled/` directory
- `[BUNDLE]` is then name of bundle, most likely `default`